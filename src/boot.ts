declare const __ENVIRONMENT__: any;

import { enableProdMode } from "@angular/core";
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";

import { MainModule } from "@cs/main";

if (__ENVIRONMENT__.production) {
    enableProdMode();
}

platformBrowserDynamic().bootstrapModule(MainModule);

import { Component } from "@angular/core";

@Component({
    selector: "cs-layout-footer",
    styles: [require("./styles")],
    template: require("./template")
})
export class LayoutFooter { }

import { Component, EventEmitter, Output } from "@angular/core";

@Component({
    selector: "cs-layout-header",
    styles: [require("./styles")],
    template: require("./template")
})
export class LayoutHeader {
    @Output() toggle: EventEmitter<null> = new EventEmitter();

    open() {
        this.toggle.next();
    }
}

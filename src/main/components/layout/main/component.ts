import { Component } from "@angular/core";

@Component({
    selector: "cs-layout-main",
    styles: [require("./styles")],
    template: require("./template")
})
export class LayoutMain { }

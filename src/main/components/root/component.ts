import { Component, ViewEncapsulation } from "@angular/core";

interface Menu {
    name?: string;
    items: MenuItem[];
}

interface MenuItem {
    icon: string;
    name: string;
}

@Component({
    encapsulation: ViewEncapsulation.None,
    selector: "cs-root",
    styles: [require("./_theme"), require("./styles")],
    template: require("./template")
})
export class Root {
    menus: Menu[] = [
        {
            items: [
                {
                    icon: "home",
                    name: "Home"
                }
            ]
        }
    ];
}

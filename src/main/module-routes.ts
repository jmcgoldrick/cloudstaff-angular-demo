import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MaterialModule } from "@angular/material";
import { RouterModule, Routes } from "@angular/router";

import { RouteHome } from "./routes";

const ROUTES: Routes = [
    {
        component: RouteHome,
        path: "**"
    }
];

@NgModule({
    declarations: [
        RouteHome
    ],
    exports: [
        RouterModule
    ],
    imports: [
        CommonModule,
        MaterialModule,
        RouterModule.forRoot(ROUTES)
    ]
})
export class RoutesModule { }

import { NgModule } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";
import { HttpModule } from "@angular/http";
import { MaterialModule } from "@angular/material";
import { BrowserModule } from "@angular/platform-browser";

import {
    LayoutFooter,
    LayoutHeader,
    LayoutMain,
    Root
} from "./components";

import { RoutesModule } from "./module-routes";

@NgModule({
    bootstrap: [Root],
    declarations: [
        LayoutFooter,
        LayoutHeader,
        LayoutMain,
        Root
    ],
    imports: [
        BrowserModule,
        FlexLayoutModule.forRoot(),
        HttpModule,
        MaterialModule.forRoot(),
        RoutesModule
    ]
})
export class MainModule { }

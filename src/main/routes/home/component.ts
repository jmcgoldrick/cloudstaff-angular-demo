import { Component } from "@angular/core";

@Component({
    styles: [require("./styles")],
    template: require("./template")
})
export class RouteHome { }

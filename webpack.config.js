const path = require("path");
const webpack = require("webpack");

module.exports = function (env) {
    const plugins = ["production"].includes(env) ? [new webpack.optimize.DedupePlugin(), new webpack.optimize.UglifyJsPlugin()] : [];

    return {
        context: path.resolve(__dirname, "src"),
        entry: {
            "client": [
                "./boot.ts"
            ],
            "polies": [
                "core-js/shim",
                "zone.js/dist/zone"
            ],
            "vendor": [
                "@angular/common",
                "@angular/compiler",
                "@angular/core",
                "@angular/flex-layout",
                "@angular/forms",
                "@angular/http",
                "@angular/material",
                "@angular/platform-browser",
                "@angular/platform-browser-dynamic",
                "@angular/router",
                "hammerjs",
                "rxjs"
            ]
        },
        devServer: {
            compress: true,
            contentBase: path.resolve(__dirname, "www"),
            historyApiFallback: true,
            stats: "minimal"
        },
        devtool: "#inline-source-map",
        module: {
            exprContextCritical: false,
            rules: [
                {
                    test: /\.css$/,
                    use: ["raw-loader", {
                        loader: "postcss-loader",
                        options: {
                            plugins: () => {
                                return [require("postcss-cssnext")()];
                            }
                        }
                    }]
                },
                {
                    test: /\.html$/,
                    use: ["raw-loader"]
                },
                {
                    test: /\.scss$/,
                    use: ["raw-loader", {
                        loader: "postcss-loader",
                        options: {
                            plugins: () => {
                                return [require("postcss-cssnext")()];
                            }
                        }
                    }, "sass-loader"]
                },
                {
                    test: /\.ts$/,
                    use: ["awesome-typescript-loader"]
                }
            ]
        },
        output: {
            filename: "[name]-bundle.js",
            path: path.resolve(__dirname, "www")
        },
        performance: {
            hints: ["production"].includes(env) ? "warning" : false
        },
        plugins: [
            new webpack.optimize.CommonsChunkPlugin({
                filename: "[name]-bundle.js",
                minChunks: Infinity,
                name: ["vendor", "polies"]
            }),
            new webpack.DefinePlugin({
                __ENVIRONMENT__: {
                    [env]: true
                }
            }),
            new webpack.NormalModuleReplacementPlugin(/@cs\/config/, "@cs/config/" + env)
        ].concat(plugins),
        resolve: {
            alias: {
                "@cs": path.resolve(__dirname, "src")
            },
            extensions: [".ts", ".js", ".html", ".scss", "*"],
            modules: [path.resolve(__dirname, "src"), "node_modules"]
        },
        watchOptions: {
            ignored: /node_modules/
        }
    };
}
